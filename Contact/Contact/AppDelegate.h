//
//  AppDelegate.h
//  Contact
//
//  Created by STUDENT on 9/24/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

