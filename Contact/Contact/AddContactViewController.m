//
//  AddContactViewController.m
//  Contact
//
//  Created by STUDENT on 9/24/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "AddContactViewController.h"
#import "ContactListTableViewController.h"
#import "Contact.h"

@interface AddContactViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveContactButton;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end

@implementation AddContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setContact:(Contact *)contact {
    contact.name = self.nameTextField.text;
    contact.phone = self.phoneTextField.text;
    contact.email = self.emailTextField.text;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqual:@"unwindAddContact"]) {
        ContactListTableViewController *contactListViewController = [segue destinationViewController];
        Contact *contact = [[Contact alloc] init];
        [self setContact:contact];
        [contactListViewController addContact:contact];
    }
}


@end
