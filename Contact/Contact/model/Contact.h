//
//  Contact.h
//  Contact
//
//  Created by STUDENT on 9/24/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *imageName;

@end
