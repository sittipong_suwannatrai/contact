//
//  ContactList.m
//  Contact
//
//  Created by STUDENT on 9/24/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "ContactList.h"
#import "Contact.h"

@implementation ContactList {
    NSMutableArray *contactList;
}

- (id)init {
    self = [super init];
    if (self) {
        [self loadData];
    }
    return self;
}

- (void)loadData {
    contactList = [[NSMutableArray alloc] init];
}

- (Contact *)getContactAtIndex:(NSUInteger)index {
    return contactList[index];
}

- (void)addContact:(Contact *)contact {
    [contactList addObject:contact];
}

- (void)addContactWithName:(NSString *)name phone:(NSString *)phone {
    Contact *contact = [[Contact alloc] init];
    contact.name = name;
    contact.phone = phone;
    [contactList addObject:contact];
}

- (NSUInteger)count {
    return [contactList count];
}

@end
