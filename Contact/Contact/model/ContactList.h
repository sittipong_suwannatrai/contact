//
//  ContactList.h
//  Contact
//
//  Created by STUDENT on 9/24/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Contact;

@interface ContactList : NSObject

- (Contact *)getContactAtIndex:(NSUInteger)index;
- (void)addContact:(Contact *)contact;
- (void)addContactWithName:(NSString *)name phone:(NSString *)phone;
- (NSUInteger)count;

@end
