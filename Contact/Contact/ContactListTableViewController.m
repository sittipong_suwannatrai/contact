//
//  ContactListTableViewController.m
//  Contact
//
//  Created by STUDENT on 9/24/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "ContactListTableViewController.h"
#import "ContactList.h"
#import "Contact.h"

@interface ContactListTableViewController ()

@property (strong, nonatomic) IBOutlet UITableView *contactTableView;


@end

@implementation ContactListTableViewController {
    ContactList *contactList;
}

- (void)loadData {
    contactList = [[ContactList alloc] init];
    [contactList addContactWithName:@"Sittipong Suwannatrai" phone:@"0891335574"];
    [contactList addContactWithName:@"Supachai Chankate" phone:@"0869840737"];
    NSLog(@"Contact number %lul", [contactList count]);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)addContactCell {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[contactList count]-1 inSection:0];
    [self.contactTableView beginUpdates];
    [self.contactTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.contactTableView endUpdates];
}

- (void)addContact:(Contact *)contact {
    [contactList addContact:contact];
}

- (IBAction)unwindAddContact:(UIStoryboardSegue *)segue {
    [self addContactCell];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [contactList count];
}

- (void)setCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Contact *contact = [contactList getContactAtIndex:indexPath.row];
    cell.textLabel.text = contact.name;
    cell.detailTextLabel.text = contact.phone;
    cell.imageView.image = [UIImage imageNamed:contact.imageName];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell" forIndexPath:indexPath];
    
    [self setCell:cell atIndexPath:indexPath];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
